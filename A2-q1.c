#include <stdio.h>

int main()
{
	double n1;

	printf("Enter your number :");
	scanf("%lf", &n1);

	if (n1>=0){
		if (n1 == 0)
			printf("Zero\n");
		else
			printf("Positive Number\n");
	}
	else 
		printf("Negative Number\n");

	return 0;
}
