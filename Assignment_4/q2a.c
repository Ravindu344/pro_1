#include <stdio.h>
#include <math.h>
int main(void)
{
	float r, h, v;


	printf("Insert the size of the Radius(cm): ");
	scanf("%f", &r);

	printf("Insert the size of the Height(cm): ");
	scanf("%f", &h);

	printf("\n");
	printf("Radius of the Cone(cm): %f\n", r);
	printf("Height of the Cone(cm): %f\n", h);

	v = (22 * r * r * h) / (3 * 7);
	
	printf("\n");
	printf("Volume of Cone(cm^3) = %f\n", v);
	return 0;
}
