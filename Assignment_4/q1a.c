#include <stdio.h>

int main(void)
{
	int n1, n2, n3, sum, ave;
	
	printf("Enter 1st Number: ");
	scanf("%d", &n1);
	
	printf("Enter 2nd Number: ");
	scanf("%d", &n2);
	
	printf("Enter 3rd Number: ");
	scanf("%d", &n3);
	
	printf("Numbers you have entered: %d, %d, %d\n", n1, n2, n3);
	sum = n1 + n2 + n3;
	
	printf("Sum of the Numbers you entered: %d\n", sum);
	
	ave = sum / 3;
	
	printf("Average of the Numbers you entered: %d\n", ave);
	
	return 0;
}