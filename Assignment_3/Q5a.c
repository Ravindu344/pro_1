#include <stdio.h>

int main()
{
	int i, j, n;

	printf("Input table number: ");
	scanf("%d", &n);

	printf("Multiplication table from 1");

	for(i = 1; i <= n; i++)
	{
		for( j = 1; j  <= 10; j++)
		{
			if (j <= n - 1)
				printf("%d x %d = %d\n", i, j, i * j);
			else
				printf("%d x %d = %d\n", i, j, i *  j);
		}
		printf("\n");
	}
	return 0;
}
