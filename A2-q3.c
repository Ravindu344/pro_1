#include <stdio.h>

int main()
{
	char C;
	int lowercase, uppercase;

	printf("Enter a Letter:");
	scanf("%c", &C);

	//evaluate to 1 if variabel C is lowercase
	lowercase = (C == 'a'|| C == 'e'|| C == 'i'|| C == 'o'|| C == 'u');

	//evaluate to 1 if variable is uppercase 
	uppercase = (C == 'A' || C == 'E' || C == 'I' || C == 'O' || C == 'U');

	//evaluates to 1 if c is either lowercase or uppercase
	if (lowercase || uppercase)
		printf("%c is a vowel.\n", C);
	else
		printf("%c is a consonant.\n", C);
	
	return 0;
}
