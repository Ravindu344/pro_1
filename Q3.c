/*Swapping Integers*/
#include <stdio.h>

int main(void)
{
	int a, b, c;

	printf("Enter first Integer:\n");
	scanf("%d", &a);

	printf("Enter second Integer:\n");
	scanf("%d", &b);
	
	printf("Before Swap\n1st Integer = %d\n2nd Integer = %d\n", a, b);

	c = a;
        a = b;
       	b = c;

	printf("After Swap\n1st Integer = %d\n2nd Integer = %d\n", a, b);
	return 0;
}
