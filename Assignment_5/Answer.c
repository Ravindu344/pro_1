#include <stdio.h>

int atnd(int tkt){
	return(120 - ((tkt - 15) / 5 * 20));
}

int incm(int tkt){
	return(tkt * atnd(tkt));
}

int xpend(int tkt){
	return(500 + 3 * atnd(tkt));
}

int prof(int tkt){
	return(incm(tkt) - xpend(tkt));
}

int main()
{
	int tkt;
	printf("Insert the ticket price: ");
	scanf("%d", &tkt);
	printf("\n");
	printf("\n");
		
	printf("There will be %d of attendees.\n", atnd(tkt));
	printf("Your income = %d\n", incm(tkt));
	printf("Total cost for the performanc = %d\n", xpend(tkt));
	printf("The total profit you earn = %d\n", prof(tkt));

	printf("\n");
	printf("\n");

	return 0;
}

