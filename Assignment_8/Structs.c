#include<stdio.h>

struct student
{
    char FName[20];
    char Subject[10];
    int Marks;
};

void instertData(struct student *st);

int main()
{
    int t;

    printf("Number of the Students: ");
    scanf("%d", &t);
    printf("\n");

    struct student data[t];
    for(int i=0; i<sizeof(data)/sizeof(struct student); i++)
    {
        instertData(&data[i]);
    }
    printf("---------------------------------------------\n");
    printf("---------------------------------------------\n");
    for(int j = 0; j<sizeof(data)/sizeof(struct student); j++)
    {
        printf("********************************************************\n");
        printf("Student number %d\n", j+1);
        printf("\n");
        printf("Name of the student               : %s\n", data[j].FName);
        printf("Subject of the student            : %s\n", data[j].Subject);
        printf("Marks he/she gained to the subject: %d\n", data[j].Marks);
        printf("********************************************************\n");
    }
    return 0;
}

void instertData(struct student *st)
{
    printf("Insert Student Name:");
    scanf(" %[^\n]%*c", &st->FName);
    printf("Insert the Subject:");
    scanf(" %[^\n]%*c", &st->Subject);
    printf("Insert Marks gained:");
    scanf("%d", &st->Marks);
    printf("\n");
}


