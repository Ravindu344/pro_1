#include <stdio.h>

int main()
{
	int a, b;

	printf("Insert an 1st Integer: ");
	scanf("%d", &a);

	printf("Insert an 2nd Integer: ");
	scanf("%d", &b);

	printf("\n");
	printf("1st Integer before swap: %d\n", a);
	printf("2nd Integer before swap: %d\n", b);

	a = a + b;
	b = a - b;
	a = a - b;

	printf("\n");
	printf("1st Integer after swap: %d\n", a);
	printf("2nd Integer after swap: %d\n", b);

	return 0;
}