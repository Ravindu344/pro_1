#include<stdio.h>
#include<string.h>
int main()
{
    int k = 0, r1 = 0, c1 = 0, r2 = 0, c2 = 0;
    printf("Matrix 1,\n");
    printf("Enter the number of rows: ");
    scanf("%d", &r1);
    printf("Enter the number of columns: ");
    scanf("%d", &c1);
    printf("\nMatrix 2,\n");
    printf("Enter the number of rows:");
    scanf("%d", &r2);
    printf("Enter the number of columns:");
    scanf("%d", &c2);
    printf("\n\n");

    int a[r1][c1], b[r2][c2];
    printf("Insert the elemants of the first Matrix:\n");
    for(int i=0; i < r1; i++)
    {
        for(int j=0; j < c1; j++)
        {
            printf("Insert the value of a%d%d = ", i+1, j+1);
            scanf("%d",&a[i][j]);
        }
    }

    printf("\nInsert the elements of the Second Matrix:\n");
    for(int i=0; i < r2; i++)
    {
        for(int j=0; j < c2; j++)
        {
            printf("Insert the value of b%d%d = ", i+1, j+1);
            scanf("%d",&b[i][j]);
        }
    }
    printf("\n\n");

    printf("First matrix: \n");
    for(int i=0; i < r1; i++)
    {
        printf("|");
        for(int j=0; j < c1; j++)
        {
            printf("%4d",a[i][j]);
        }
        printf("%4c|\n",'\0');
    }
    printf("\n");
    printf("Second matrix: \n");
    for(int i = 0; i < r2; i++)
    {
        printf("|");
        for(int j = 0; j < c2; j++)
        {
            printf("%4d",b[i][j]);
        }
        printf("%4c|\n",'\0');
    }
    printf("\n\n");

    if(r1 == r2 && c1 == c2)
    {
        int output[r1][c1];
        for(int i = 0; i < r1; i++)
        {
            for(int j = 0; j < c1; j++)
            {
                output[i][j] = a[i][j] + b[i][j];
            }
        }
        printf("\nAddition of the matrixes:\n");
        for(int i = 0; i < r2; i++)
        {
            printf("|");
            for(int j = 0; j < c2; j++)
            {
                printf("%4d",output[i][j]);
            }
            printf("%4c|\n",'\0');
        }
    }
    else
    {
        printf("Can't add matrices.");
    }
    printf("\n\n");

    if(c1 == r2)
    {
        int output2[r1][c2];
        for(int i = 0; i < r1; i++)
        {
            for(int j = 0; j < c2; j++)
            {
                output2[i][j]=0;
            }
        }
        for (int i = 0; i < r1; i++)
        {
            for (int j = 0; j < c2; j++)
            {
                for (int k = 0; k < c1; k++)
                {
                    output2[i][j] += a[i][k] * b[k][j];
                }
            }
        }
        printf("\nMultiplication of the matrixes:\n");
        for(int i = 0; i < r1; i++)
        {
            printf("|");
            for(int j = 0; j < c2; j++)
            {
                printf("%4d",output2[i][j]);
            }
            printf("%4c|\n",'\0');
        }
    }
    else
    {
        printf("Cannot multiply matrixes.\n\n");
    }
    return 0;
}