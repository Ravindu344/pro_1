#include <stdio.h>

int main()
{
	float c, f;

	printf("Insert the Temperature (°C): ");
	scanf("%f", &c);

	printf("Temperature (°C): %.2f\n", c);

	f = (c * 9 / 5) + 32;

	printf("Temperature (°F): %.2f\n", f);
	return 0;
}
