#include <stdio.h>

int main(void)
{
	int n1;

	printf("Enter your number =");
	scanf("%d", &n1);
	
	if (n1%2 == 0)
		printf("It is an even number.\n");
	else
		printf("It is an odd number.\n");

	return 0;
}
