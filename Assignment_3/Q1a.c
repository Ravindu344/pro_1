#include <stdio.h>

int main()
{
	int Num1 = 0, total = 0;
	do{
		printf("Enter a Positive number = ");
		scanf("%d", &Num1);

		if(Num1 <= 0)	break;
		total += Num1;
	}
	while(Num1 > 0);

	printf("Total = %d\n", total);
	return 0;
}
