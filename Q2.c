/*Computing the area*/
#include <stdio.h>
#define PI 3.14159f

int main(void)
{
	float r, area;
	
	printf("Enter the size of the Radius(cm):");
	scanf("%f", &r);

	area = PI * r * r;
	printf("Area of the Disc(square cm): %f", area);
	
	return 0;
}
