#include <stdio.h>

int printb(int);

int main()
{
    unsigned int A =10, B = 15, C = 0;

    C =A&B;

    printf("A = %d (%d)\n", A, printb(A));
    printf("B = %d (%d)\n", B, printb(B));

    printf("\n");
    printf("A & B = %d (%d)\n", C, printb(C));

    C = A | B;

    printf("\n");
    printf("A | B = %d (%d)\n", C, printb(C));

    C = A ^ B;

    printf("\n");
    printf("A ^ B = %d (%d)\n", C, printb(C));

    C = ~A;

    printf("\n");
    printf("~A = %d (%d)\n", C, printb(C));

    C = A << 3;

    printf("\n");
    printf("A << 3 = %d (%d)\n", C, printb(C));

    C = B >> 3;

    printf("\n");
    printf("B >> 3 = %d (%d)\n", C, printb(C));
    return 0;
}

int printb(int n){
    if (n == 0) return 0;
    else return (n % 2) + 10 * printb(n / 2);
}