/*read and multiply two floating point numbers*/
#include <stdio.h>

int main(void)
{
	float number_1, number_2, multiply;
	
	printf("Enter first Floating point number:");
	scanf("%f", &number_1);

	printf("Enter second floating point number:");
	scanf("%f", &number_2);

	multiply = number_1 * number_2;

	printf("Answer= %f\n", multiply);
	return 0;
}
